# Push Notifications Integration - Guide

## Overview

This repository contains the integration of push notifications within the context of the room management feature of our application. The following guide provides screenshots and step-by-step instructions to facilitate the usage of this functionality.

## Screenshots

1. **Room Overview Page:**
   - Navigate to the Room Overview page.
   - Expand the "Options" menu.
   - Select "Push Notification."

   ![Push Notification - Room Overview](https://git.thm.de/mnaa40/frag.jetzt/-/raw/4d2e5d06e2814d250bf1568fb51d468cd5e742da/Screenshots/p1.png)

2. **Room Page:**
   - Navigate to the Room page.
   - Expand the "Options" menu.
   - Select "My Profile."
   - Expand the "Push Notification" menu.

   ![Push Notification - Room Page](https://git.thm.de/mnaa40/frag.jetzt/-/raw/4d2e5d06e2814d250bf1568fb51d468cd5e742da/Screenshots/p2.png)

3. **Filter Options:**
   - Take note of the filter options in the screenshots.
   - By default, all options are selected.

   ![Filter Options](https://git.thm.de/mnaa40/frag.jetzt/-/raw/4d2e5d06e2814d250bf1568fb51d468cd5e742da/Screenshots/p3.png)

## Guide

1. **Room Overview Page:**
   - Navigate to the Room Overview page.
   - Expand the "Options" menu.
   - Select "Push notification."

2. **Room Page:**
   - Navigate to the Room page.
   - Expand the "Options" menu.
   - Select "My Profile."
   - Expand the "Web Notification" menu.

3. **Filter Options:**
   - Take note of the filter options in the screenshots.
   - By default, all options are selected.

## Development Guide

If you want to contribute to the development of this feature, follow these steps:

1. Clone the repository: `git clone https://git.thm.de/mnaa40/frag.jetzt.git`
2. Navigate to the project directory: `cd your-repo`
3. Install dependencies: `./.docker/setup.sh`
4. Run the application: `docker compose up`

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
