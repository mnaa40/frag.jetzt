import { Component, Input, OnInit } from '@angular/core';
import { DashboardNotificationService } from '../../../../services/util/dashboard-notification.service';
import { UUID } from '../../../../utils/ts-utils';
import { MatDialogRef } from '@angular/material/dialog';
import { InterestBit } from '../../../../services/http/change-subscription.service';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-push-notification-interest',
  templateUrl: './push-notification-interest.component.html',
  styleUrls: ['./push-notification-interest.component.scss'],
})
export class PushNotificationInterestComponent implements OnInit {
  @Input()
  roomId: UUID;

  listenToCreated = false;
  listenToDeleted = false;
  listenToAnswered = false;
  listenToChangeAck = false;
  listenToChangeFavorite = false;
  listenToChangeCorrect = false;
  listenToChangeTag = false;
  listenToChangeScore = false;

  constructor(
    private dashboardService: DashboardNotificationService,
    private dialogRef: MatDialogRef<PushNotificationInterestComponent>,
  ) {}

  ngOnInit(): void {}
  submit() {
    const interestBits =
      (this.listenToCreated ? InterestBit.CREATED : 0) |
      (this.listenToDeleted ? InterestBit.DELETED : 0) |
      (this.listenToAnswered ? InterestBit.ANSWERED : 0) |
      (this.listenToChangeAck ? InterestBit.CHANGE_ACK : 0) |
      (this.listenToChangeFavorite ? InterestBit.CHANGE_FAVORITE : 0) |
      (this.listenToChangeCorrect ? InterestBit.CHANGE_CORRECT : 0) |
      (this.listenToChangeTag ? InterestBit.CHANGE_TAG : 0) |
      (this.listenToChangeScore ? InterestBit.CHANGE_SCORE : 0);
    /*    this.dashboardService
      .addRoomSubscription(this.roomId, interestBits)
      .subscribe();*/
    if (this.dashboardService.hasRoomSubscription(this.roomId)) {
      this.dashboardService
        .deleteRoomSubscription(this.roomId)
        .pipe(
          concatMap(() =>
            this.dashboardService.addRoomSubscription(
              this.roomId,
              interestBits,
            ),
          ),
        )
        .subscribe({
          next: (response) => {
            console.log('Re-subscribed to room:', response);
          },
          error: (error) => {
            console.error('Error during subscription process:', error);
          },
        });
    } else {
      this.dashboardService
        .addRoomSubscription(this.roomId, interestBits)
        .subscribe({
          next: (response) => {
            console.log('Subscribed to room:', response);
          },
          error: (error) => {
            console.error('Error subscribing to room:', error);
          },
        });
    }
    this.dialogRef.close();
  }
}
